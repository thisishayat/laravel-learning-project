<?php

namespace App\Http\Controllers;

use App\Repositories\Todo\TodoRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class TodoController extends Controller
{
    //
    /**
     * @var TodoRepository
     */
    private $todo;

    public function __construct(TodoRepository $todo)
    {
        $this->todo = $todo;
    }
    public function getAllTodos(){
        return $this->todo->getAll();
    }
}
