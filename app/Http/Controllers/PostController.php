<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return "its working";
        $posts = Post::all();
        $hayat = 1;
        return view('formPosts.index',compact('posts','hayat'));
        //$url = route('userroute');
        //return $url;

//        $url = route('userid',['id' => 1]);
//        return $url;
        
    }

    public function create()
    {
        //
        return view('formPosts.create');
    }


    public function store()
    {
        //
        //all();
        //echo "form post is ok";
        //Post::create(array('title' => Input::get('title'))); // when use post problem occur


        $postinsert = new Post;
        $postinsert->title = Input::get('title');
        $postinsert->save();


        $posts = Post::all();
        $hayat = 2;
        return view('formPosts.index',compact('posts','hayat'));

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post_edit = Post::findOrFail($id);
        return view('formPosts.show', compact('post_edit'));
       
    }

    /**
     * Show the  form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::findOrFail($id);
        return view('formPosts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //return "its working";
        //return $request->all();

        $post = Post::findorFail($id);
        $post->update($request->all());
        return redirect('formPosts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //return "ITS Working";
        $post = Post::findOrFail($id);
        $post->delete();
        return "deleted successfully";


    }

    public function contactPage(){
        $people = ['Edwin','Paul','Hayat','Jasim'];
        return view('contact', compact('people'));
    }
    public function postPage($id,$name,$password){
        return view('post', compact('id','name','password'));
    }
}
