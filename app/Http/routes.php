<?php
namespace App\Http\Controllers;
use App\Country;
use App\Post;
use App\User;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return view('welcome');
    return 'This is hayat. Welcome to laravel beginner project';
});

Route::get('/person', function () {

    return 'this is person page';
});

Route::get('/contact', function () {

    return 'this is contact page';
});

Route::get('/post/{id}/{name}', function($id,$name){
        return "this is test post".$id." ".$name;
    });
Route::get('admin/posts/hello', array('as' => 'admin.home', function(){
    $url = route('admin.home');
    return "this url is"." ".$url;
}));

Route::get('/post/{id}','PostController@index');
Route::get('/contact','PostController@contactPage');
Route::get('/post/{id}/{name}/{password}','PostController@postPage');

/*
=============================================
Insert value
=============================================
*/
Route::get('/insert', function(){
    DB::insert('INSERT INTO posts(title,body) values(?,?)', ['Tell Me','Hello this is hayat']);
});
/*
=============================================
Read value
=============================================
*/
Route::get('/read', function(){

    $results = DB::select('SELECT * FROM posts WHERE id=?',[1]);
    //return $results;

    foreach($results as $post){
        $title = $post->title;
        $content = $post->body;
        echo $title;
        echo $content;
    }
});
/*
=============================================
Update value
=============================================
*/
Route::get('/update', function(){
    $updated = DB::update('UPDATE posts SET title="title changed 2" WHERE id=?',[1]);
    return $updated;
});
/*
=============================================
Delete value
=============================================
*/
Route::get('/delete', function(){
    $deleted = DB::delete('DELETE FROM posts WHERE id=?', [2]);
    return $deleted;
});
/*
=============================================
Eloquent value insert
=============================================
*/
Route::get('/readSecond', function(){

    $posts = Post::all();
    foreach ($posts as $row){
        return $row->title;
    }
});
/*
===========================  ==================
Eloquent value find
=============================================
*/
Route::get('/find', function(){

    $posts = Post::find(1);
    //$posts = post::find(1); // it will also work
    return $posts->title;

});
/*
=============================================
Basic insert value eloquent
=============================================
*/
Route::get('/basicInsert', function(){
    $post = new Post;

    $post->title = "eloquent title goes here";
    $post->body = "eloquent body goes here";

    $post->save();
});

/*
=============================================
test php file run
=============================================
*/

Route::get('/hayatphp', function(){
    return view('hayat');
});

/*
=============================================
test php file run
=============================================
*/

Route::get('/zahid', function(){
    return view('zahid');
});
/*
=============================================
test php file run
=============================================
*/

Route::get('/classroom', function(){
    return view('freemig\home\canvas\classrooms\classrooms');
});

/*
=============================================
Creating and configuring mass insert value eloquent
=============================================
*/
Route::get('/creatingData',function(){
    Post::create(['title'=>'here goes title','body'=>'here goes body']);
});
/*
=============================================
Updating eloquent
=============================================
*/
Route::get('/updateEloquent',function(){
    Post::where('id',3)->update(['title'=>'title changed here','body'=>'body changed here']);
});
/*
=============================================
Updating eloquent
=============================================
*/
Route::get('/deleteEloquent',function(){
    //Post::find(3)->delete();
    Post::destroy(4);

});
/*
=============================================
Soft Delete eloquent
=============================================
*/
Route::get('/softDeleteEloquent',function(){

    Post::find(6)->delete();
});
/*
=============================================
Soft Delete restore eloquent
=============================================
*/
Route::get('/softDeleteRestore',function(){

    //return Post::find(5);
//    $post = Post::withTrashed()->where('id',6)->get();
//    return $post;
    $post = Post::onlyTrashed()->where('title','eloquent title goes here')->get();
    return $post;
});

/*
=============================================
Soft Delete restore 2 eloquent
=============================================
*/
Route::get('/softDeleteRestore2',function(){

     Post::withTrashed()->where('title','eloquent title goes here')->restore();

});
/*
=============================================
Soft Delete permanent delete
=============================================
*/
Route::get('/forceDelete',function(){

    Post::onlyTrashed()->where('title','eloquent title goes here')->forceDelete();

});
/*
=============================================
one to one eloquent relationship
=============================================
*/
Route::get('user/{id}/post', function($id){
    return User::find($id)->post;
});
/*
=============================================
one to many eloquent relationship
=============================================
*/
Route::get('/posts', function(){
    $user = User::find(1);

    foreach ($user->posts as $post){
        echo $post->title."<br>";
    }
});
/*
=============================================
many to many eloquent relationship
=============================================
*/
Route::get('user/{id}/role', function($id){
    $user = User::find($id);

    foreach ($user->roles as $role){
        return $role->name;
    }
});

/*
=============================================
Accessing the intermediate table and private
=============================================
*/
Route::get('user/private', function(){
    $user = User::find(1);

    foreach (User::find(1)->roles as $role){
        echo $role->pivot->created_at;
    }
});
/*
=============================================
Has many through relation
=============================================
*/
Route::get('user/country', function(){
    $country = Country::find(1);

    foreach ($country->posts as $post){
            echo $post->title;
    }
});
/*
=============================================
CRUD application
=============================================
*/
Route::get('formPosts/create', 'PostController@create');
Route::get('formPosts', 'PostController@index');
Route::get('formPosts/{id}', 'PostController@show');
Route::get('formPosts/{id}/edit', 'PostController@edit');
Route::get('formPosts/{id}/update', 'PostController@update');
Route::get('formPosts/{id}/destroy', 'PostController@destroy');
Route::post('formPosts/store', 'PostController@store'); // when use post problem occur.


Route::get('usertest/{id}/profile', ['as' => 'profile', function ($id) {
    //
    $url = route('profile', ['id' => 1]);
    return $url;
}]);

Route::get('formPosts', [
    'as' => 'userid',
    'uses' => 'PostController@index'
]);

Route::get('mytodo', 'TodoController@getAllTodos' );


