<?php
/**
 * Created by PhpStorm.
 * User: HAYAT
 * Date: 6/6/2016
 * Time: 4:31 PM
 */

namespace App\Repositories\Todo;


use App\Todo;

/**
 * @property Todo model
 */
class ElequentTodo implements  TodoRepository
{

    public function __construct(Todo $model){
        $this->model = $model;
    }



    /**
     * @return mixed
     */
    public function getAll()
    {
        // TODO: Implement getAll() method.

        return $this->model->all();
    }
}