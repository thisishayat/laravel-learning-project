<?php
/**
 * Created by PhpStorm.
 * User: HAYAT
 * Date: 6/6/2016
 * Time: 4:29 PM
 */

namespace App\Repositories\Todo;


interface TodoRepository
{
    public function getAll();
}