<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
/*
 =====================================
 one to one relationship
 =====================================
*/
    public function post(){
        return $this->hasOne('App\Post'); // back slash is important here
    }

/*
 =====================================
 many to many relationship
 =====================================
*/
    public function roles(){
        return $this->belongsToMany('App\Role')->withPivot('created_at'); // back slash is important here
        // to customize tables name and columns
        // return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }
}


