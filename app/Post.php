<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    // creating data and configuring mass assignment

    
    protected $fillable=[
        'title','body',
    ];
}
